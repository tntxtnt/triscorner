#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Tri Tran'
SITENAME = "Tri's Corner"
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Chicago'

DEFAULT_LANG = 'English'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
)

# Social widget
SOCIAL = (('Atom feed', '/feeds/all.atom.xml', 'rss'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

USE_FOLDER_AS_CATEGORY = True
IGNORE_FILES = ['skeleton.md']
STATIC_PATHS = ['static', '404.html', '.well-known/acme-challenge']
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'tri_pretty_md': {},
    },
    'output_format': 'html5'
}
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['pelican_dynamic', 'i18n_subsites', 'tag_cloud']
THEME = './theme/pelican-bootstrap3'
BOOTSTRAP_THEME = 'yeti'
PYGMENTS_STYLE = 'silver'
SITELOGO = 'static/images/TT.png'
SITELOGO_SIZE = 20
FAVICON = 'static/images/TT.png'
DISPLAY_TAGS_ON_SIDEBAR = True
BANNER_CSS_FILE = 'static/css/banner.css'
BANNER_ALL_PAGES = False
CUSTOM_CSS = 'static/css/custom.css'
DISPLAY_TAGS_INLINE = True
USE_OPEN_GRAPH = False
DISPLAY_ARCHIVE_ON_SIDEBAR = True
DISPLAY_CATEGORIES_ON_SIDEBAR = False
DISPLAY_RECENT_POSTS_ON_SIDEBAR = True
THEME_STATIC_DIR = 'static'
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}
#EXTRA_PATH_METADATA = {
#    'static/favicon.ico': {'path': 'favicon.ico'}
#}
ARCHIVES_URL = 'archives/'
ARTICLE_URL = 'post/{slug}/'
AUTHOR_URL = 'author/{slug}/'
AUTHORS_URL = 'authors/'
CATEGORY_URL = 'category/{slug}/'
CATEGORIES_URL = 'categories/'
DRAFT_URL = 'draft/{slug}/'
MONTH_ARCHIVE_URL = 'archive/{date:%Y}/{date:%m}/'
PAGE_URL = 'page/{slug}/'
TAG_URL = 'tag/{slug}/'
TAGS_URL = 'tags/'

ARCHIVES_SAVE_AS = 'archives/index.html'
ARTICLE_SAVE_AS = 'post/{slug}/index.html'
AUTHOR_SAVE_AS = 'author/{slug}/index.html'
AUTHORS_SAVE_AS = 'authors/index.html'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
CATEGORIES_SAVE_AS = 'categories/index.html'
DRAFT_SAVE_AS = 'draft/{slug}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archive/{date:%Y}/{date:%m}/index.html'
PAGE_SAVE_AS = 'page/{slug}/index.html'
TAG_SAVE_AS = 'tag/{slug}/index.html'
TAGS_SAVE_AS = 'tags/index.html'

READERS = {"html": None}