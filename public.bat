@REM publish.bat
@ECHO off

SET startpelican="C:\Users\tri\pelican\Scripts\activate"

REM Start virtualenv
CALL %startpelican%

REM Start publish build
pelican content -o public -s publishconf.py
