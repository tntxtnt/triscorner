@REM dev-server.bat
@ECHO off

SET startpelican="C:\Users\tri\pelican\Scripts\activate"
SET port=21489
SET browser="C:\Program Files (x86)\Mozilla Firefox\firefox.exe"

REM Start virtualenv
CALL %startpelican%

REM Start regenerate build
START "" /B pelican -r content -o output -s pelicanconf.py

REM Start server
CD output
START "" /B python -m pelican.server %port%
CD ..

REM Open local site in browser
START "" %browser% "http://localhost:%port%"