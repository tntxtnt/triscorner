renderMathInElement(
  document.getElementById("article-content"),
  {
    delimiters: [
      {left: "\\[", right: "\\]", display: true},
      {left: "\\(", right: "\\)", display: false}
    ]
  }
);