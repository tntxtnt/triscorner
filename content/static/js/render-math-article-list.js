var summaries = document.getElementsByClassName("summary");

for (var i = 0; i < summaries.length; ++i)
{
  renderMathInElement(summaries[i], {
      delimiters: [
        {left: "\\[", right: "\\]", display: true},
        {left: "\\(", right: "\\)", display: false}
      ]
    }
  );
}
