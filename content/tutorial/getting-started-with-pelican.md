---
typora-root-url: ..
Title: Getting Started with Pelican on Windows 10
Date: 2017-10-26 12:00
Tags: pelican, python, static, windows
Scripts: katex.min.js, auto-render.min.js, render-math-article.js
Styles: katex.min.css
---


## Installation

- Get [Python 3](https://www.python.org/downloads/). Don't even think about Python 2, unless you need some plugins that don't support Python 3 (there won't be any).

- Install [virtualenv](https://virtualenv.pypa.io/en/stable/installation/): In command line run `pip install virtualenv`. Virtualenv let you create a new shiny clean Python environment.

- Create new virtualenv:`virtualenv pelican`. Wait 15-30 seconds.

- Enable it: `pelican\Scripts\activate`. If you have trouble then try adding its extension`.bat`

- After enabling pelican virtualenv, check with `pip freeze` and make sure it doesn't print out anything, since this is a clean environment.

- Install Pelican and Markdown: `pip install pelican markdown`. Wait 1-2 minutes and you're done!

- Run `pip freeze` again. You should see all installed packages. Now you can save them to a text file with `pip freeze > requirements.txt`, and if you need to create new environment elsewhere you can just include requirements.txt and run `pip install -r requirements.txt` and get the exact same environment. That's why you need to create new virtualenv: to get a clean requirements.txt.


## Your very first blog

In pelican environment, run `pelican-quickstart`. Remember to say **no** to Makefile and auto-reload script, since we're on Windows, and those two are for Linux environment. We will write our own batch file instead.

```text
Welcome to pelican-quickstart v3.7.1.

This script will help you create a new Pelican-based website.

Please answer the following questions so this script can generate the files
needed by Pelican.


> Where do you want to create your new web site? [.] myblog
> What will be the title of this web site? Tri's Corner
> Who will be the author of this web site? Tri Tran
> What will be the default language of this web site? [English]
> Do you want to specify a URL prefix? e.g., http://example.com   (Y/n)
> What is your URL prefix? (see above example; no trailing slash) tritran.xyz
> Do you want to enable article pagination? (Y/n)
> How many articles per page do you want? [10]
> What is your time zone? [Europe/Paris] America/Chicago
> Do you want to generate a Fabfile/Makefile to automate generation and publishing? (Y/n) n
> Do you want an auto-reload & simpleHTTP script to assist with theme and site development? (Y/n) n
Done. Your new project is available at C:\Users\tri\Blogs\Pelican\tricorner
```

Your newly created blog contains only 2 folders and 2 `.py` files.

- `content` folder is the default folder where you store your articles (`*.md`). Pelican will look into this folder to convert your articles into static HTML pages.
- `output` folder is the default output folder for *local* generated static files. You don't modify anything in this folder, since it is local, not your published site. Any changes you make here won't reflect on your real website. You need this to review your site before publishing it.
- `pelicanconf.py` is your blog's configurations, such as site name, author, social widgets. If you want to  make any change or add more options to your blog this is where you add them.
- `publishconf.py` is configuration file for published site. You won't touch this file many times.

Now create a skeleton article called `skeleton.md`. You don't have to delete it, you can ignore this file later. I like to have a skeleton file so that when creating new article I don't have to remember all the article details.

```text
Date: 2000-01-13
Title: Skeleton Post
Tags: test, skeleton, ignored
Category: blog

Test post, please ignore.
```

An article needs a title and a created date. You can ignore other metadata attributes such as tags, slug, and category, but it's nice to have them.

Next, in `myblog` folder, run `pelican content` to generate your static pages. Its output is something like this:

```text
Done: Processed 1 article, 0 drafts, 0 pages and 0 hidden pages in 3.00 seconds.
```

Check your `output` folder. It will now have a bunch of `.html` files and some folders. To view them run `cd output && python -m pelican.server`, then in your browser go to [http://localhost:8000](http://localhost:8000)

<div class="alert alert-warning"  role="alert"><strong>Warning!</strong> These files and folders are not your published site. They are used for local testing.</div>

![getting-started-1](/static/images/getting-started-1.png)

The default theme is very simple. There are some unnecessary links at the bottom. You can clean them up: open `pelicanconf.py`, comment out blog roll `LINKS` and social widget `SOCIAL`, then go to `myblog` folder and run `pelican content` again.

![getting-started-2](/static/images/getting-started-2.png)

Congratulations, you now have a blog!


## Automate builds with batch file

Manually build `pelican content` after each change is very annoying, and Pelican thinks so, too. It offers a regenerate flag `-r` that automatically re-build after each change. However, you have to run a separate console to serve local content. Having 2 running consoles at the same time is also very annoying. Moreover, you have to enable Pelican virtualenv in each console. That's very very time consuming. Luckily, we can write a batch script to automate that:

```batch
@REM dev-server.bat
@ECHO off

SET startpelican="C:\Users\tri\pelican\Scripts\activate"
SET port=8000
SET browser="C:\Program Files (x86)\Mozilla Firefox\firefox.exe"

REM Start virtualenv
CALL %startpelican%

REM Start regenerate build
START "" /B pelican -r content -o output -s pelicanconf.py

REM Start server
CD output
START "" /B python -m pelican.server %port%
CD ..

REM Open local site in browser
START "" %browser% "http://localhost:%port%"
```

You have to setup 3 variables: your Pelican virtualenv activation path, your desired localhost's port, and your web browser path. First we invoke virtualenv by using `CALL`, then we start Pelican's regenerate build in another console with `START`, but instead of spawning another console, it will run within current console window thanks to `/B` flag. Next we start Pelican server and finally open our local site in chosen browser.

<div class="alert alert-warning" role="alert"><strong>Warning!</strong> These commands doesn't wait for others to finish, so sometimes you'll open your site before Pelican server is running, and your site won't show up. Wait a few seconds and F5 refresh your page.</div>

Now you can start Pelican by double clicking `dev-server.bat`, and to stop devserver, just close the console window. Super easy to use.


## Customize your blog

It's **your** blog, make it unique, make it beautiful, or make it easy to use. Here's [what you can customize with `pelicanconf.py`](http://docs.getpelican.com/en/stable/settings.html). Some customizations that I found useful are:

- `USE_FOLDER_AS_CATEGORY = True`: super useful if you don't want to specify category for every articles. I'll just create a `blog` folder inside `content` and put all my articles in there, and their category will automatically be "blog". 

    <div class="alert alert-warning" role="alert"><strong>Warning!</strong> Some folders such as `pages` or `images`  are reserved for other uses (you can customize those rules).</div>

- `IGNORE_FILES = ['skeleton.md']`: ignore specific file (this is where you ignore the earlier `skeleton.md`, or if you know regex, you can ignore files that match your regex. Pelican won't include/convert these files into static pages.

- `STATIC_PATHS = ['static']`: put all your static assets (css, js, images, ...) in this folder (relative to `content`, so this folder path is `myblog\content\static`).

- `MARKDOWN = {...}` : markdown extensions, for me code highlight with Pygments is a must!

        :::python
        MARKDOWN = {
          'extension_configs': {
              'markdown.extensions.codehilite': {'css_class': 'highlight'},
              'markdown.extensions.extra': {},
          },
          'output_format': 'html5'
        }

- Plugins: Use `PLUGIN_PATHS = ['./plugins']` to specify your plugins path, for example mine is `myblog\plugins`, and use `PLUGINS = ['render_math', 'pelican_dynamic']` to specify which plugin to use. Here I use [`render_math`](https://github.com/barrysteyn/pelican_plugin-render_math) to write math equations \\(e^{i \pi} = -1\\) with MathJax, and [`pelican_dynamic`](https://github.com/wrobstory/pelican_dynamic) to easily embed custom `css` and `js` files into my articles. Just download a zip from the Github link (or use `git` if you know how to) then extract them to plugins folder, change there name to match the name in `pelicanconf.py` file.

- One of the most important customization: choosing [theme](http://pelicanthemes.com/)! Download a custom theme, place it in `myblog\theme`, then add `THEME = './theme/your-awesome-theme'` to `pelicanconf.py`. You can even [write a theme from scratch](http://docs.getpelican.com/en/stable/themes.html) if you really, really want it to be unique. Me? I'll just download a beautiful theme, then do some modifications to make it different from others. One of my favorites is `pelican-bootstrap3` theme. For this theme you have to use `i18n_subsites` plugin and add `JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}`. 

    ![getting-started-3](/static/images/getting-started-3.png)

    ![getting-started-4](/static/images/getting-started-4.png)

    My blog looks much better now. And it's responsive!

    <div class="alert alert-info" role="alert"><strong>Tip:</strong> The easiest way to download and update plugins/themes is to clone `pelican-themes` and `pelican-plugins` repos using `git`.</div>

- `EXTRA_PATH_METADATA`: useful for setting up `favicon.ico` and/or `robots.txt`

        :::python
        EXTRA_PATH_METADATA = {
            'static/favicon.ico': {'path': 'favicon.ico'}
        }

- `[x]_SAVE_AS` and `[x]_URL`, where "x" can be AUTHOR, ARTICLE, CATEGORY, TAG, or PAGE: automatically drop the `.html` extension from URLs. Here are my settings:

        :::python
        ARTICLE_URL = 'post/{slug}'
        ARTICLE_SAVE_AS = 'post/{slug}/index.html'
        PAGE_URL = 'page/{slug}'
        PAGE_SAVE_AS = 'page/{slug}/index.html'
        CATEGORY_URL = 'category/{slug}'
        CATEGORY_SAVE_AS = 'category/{slug}/index.html'
        TAG_URL = 'tag/{slug}'
        TAG_SAVE_AS = 'tag/{slug}/index.html'
        AUTHOR_URL = 'author/{slug}'
        AUTHOR_SAVE_AS = 'author/{slug}/index.html'


## Deploy to Firebase

There are dozen of ways to publish your static blog: upload to Github, or Gitlab, or S3, or Dropbox, ... Any service that can serve static files can serve your blog. I personally like Firebase, because they offer custom domain with https for free (up to 1GB storage).

Install Firebase is fairly easy: 

- Go to [https://console.firebase.google.com](https://console.firebase.google.com/), click "Add project" to create new project
- Install `npm`
- Install firebase-cli: `npm install -g firebase-tools`
- Login to Firebase `firebase login` 
- Go to your blog's folder, run `firebase init`
- Choose "Hosting", then connect with your project created in first step

Now you need to make publish site before deploying to Firebase:

- Open `publishconf.py`, change `RELATIVE_URLS` to `True`, or change `SITEURL` to your firebaseapp domain.
- Activate Pelican virtualenv and run `pelican content -o public -s publishconf.py`

Now you're set, open `firebase.json` and make sure its content has "public" option:

```json
{
  "hosting": {
    "public": "public",
    "cleanUrls": true,
    "trailingSlash": false
  }
}
```

To make even cleaner URLs, enable `cleanUrls` and disable `trailingSlash`.

Run `firebase serve` and check if anything goes wrong. If all is good, then run `firebase deploy`. Congratulations, you now have an online blog!

  <div class="alert alert-warning" role="alert"><strong>Warning!</strong> Firebase save each deploy separately, so although it offers 1GB storage it will be consume very fast. Remember to go to project web console and delete old versions to free up space.</div>

  <div class="alert alert-info" role="alert"><strong>Tip:</strong> You can save even more space and bandwidth by optimizing your `.png` images with [`zopflipng`](https://github.com/google/zopfli/tree/master/src/zopflipng).</div>