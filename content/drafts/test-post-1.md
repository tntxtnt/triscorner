---
typora-root-url: ..
Title: Test Post #1
Date: 2017-11-10 12:00
Tags: test
Scripts: katex.min.js, auto-render.min.js, render-math-article.js
Styles: katex.min.css
Status: draft
Slug: 1
---

2\^12 = 4096,

2\^(x + y) = 4096,

2\^x+y = 4096,

2\^x + y + 1 = 4096,

2\^12 = 4096 $y^2 = x^3 + 4$,

**2\^12** = 4096 $y^2 = x^3 + 4$,

*2\^12* = 4096 $y^2 = x^3 + 4$,

H~2~O,

Fe~2~(SO~4~)~3~,

Fe~ 1 + 1 ~(SO~4~)~3~,

f~ x + y ~,

x~1~ = 11,

y~1~ + y~2~ = 1234 $y_1 + y_2$,

**y~2~** = 4096,

*y~2~* = 4096

x\^y\^z

f~x~y~~

2\^(2\^(2))

2<sup>2<sup>2</sup></sup>