---
typora-root-url: ..
Title: Migrate from Firebase to Netlify
Date: 2017-11-08 12:00
Tags: static, hosting, https
---

Before going to the main section of why I ditch Firebase, I would like to tell you about my journey to static web hosting.

In the very beginning, I knew about blogging thanks to Yahoo Blog 360°, while it was a popular trend in Vietnam. I managed to create a blog, but I didn't have anything to write, not like many other people, so my first blogging experience was pretty underwhelming. Fast forward few years, Yahoo Blog 360° died, and I toyed with Google's Blogger for a while. I uploaded a Morse code encoder/decoder applet to it when I first learned Java (later I found out that Java applet is no good). Nevertheless, since I didn't know much about HTML, CSS, Javascript, my blog was pretty generic and again, I'm not a prolific writer, so I abandoned it.

Then I came to know about static site generators (SSG) thanks to Github, and Markdown thanks to... reddit. Now I don't have to remember HTML tags anymore, Markdown takes care of it for me, and SSG will convert Markdown text into HTML, put them to a template, and make a website for me. All I have to do is finding a **static web hosting**. But what about WordPress? Well, my impression of blogging is that it's *free*, and moreover, I don't write very often, so throwing away $5 a month or even $1 a month seems like a waste to me.

Luckily, Github offers static hosting. Just follow this and this instructions and voila, you have a website at `<your-username>.github.io`. One problem: Github doesn't support custom domains HTTPS. If you want to enable HTTPS for your custom domain with Github Pages, you have to "happily" use a man-in-the-middle service like Cloudflare and hope for the best. But is HTTPS worth it? My blog is perhaps worth nothing, and it doesn't require any logins, so why HTTPS? The answer is to prevent some ISPs from injecting their ads into my blog without my consent. Moreover, I have Let's Encrypt free certificates. From $30 a year to $0 forever? Sign me up.

So now my wish list has 3 things: **free**, **custom domain**, and **HTTPS**.

I tried Amazon S3. It offers custom domain HTTPS, but I stopped using it after a few months because it requires custom DNS, and Amazon Route 53 costs like 50 cents a month. For a blog that has almost nothing valuable, an overall of 60 cents per month seems too much. And there are many other alternatives to static hosting.

Later, I found Gitlab Pages satisfy all my needs. It allows me to upload my own certificate, so I can put a green lock to my custom domain. It also has a CI like Github, so I can publish my blog with a simple `git push`. Very convenient! One problem: it doesn't redirect HTTP requests to HTTPS. Ok, I'll have some Javascript on client side to handle that. On *client* side. It was bad, very bad. If some attackers remove that piece of code from my http response, then they can inject their ads to my site. Bad bad bad. But whatever, it's free and is better than Github Pages, because it offers HTTPS, right?

I was satisfied for a few months, until I found out about [Mozilla's Observatory](https://observatory.mozilla.org/). It helps me understand about some basic web securities beyond HTTPS, like XSS attacks, content security policy, and `unsafe-eval` is evil. Thus, I add one more item to my wish list: **custom response header**. Unfortunately, Gitlab doesn't have option to customize response header. However, since my blog doesn't have user generated content, it's ok to not include those fancy header items.

<img src="/static/images/observatory-score.png" alt="observatory-score" class="img-responsive center-block"/>

Until recently, I found out Firebase satisfy all my requirements. I even recommended it in my new first [blog](/post/getting-started-with-pelican-on-windows-10) with Pelican, mainly because it has extremely fast deployment, deploy in seconds compare to Gitlab Pages 3 minutes, automatic HTTPS renewal, prettifying URLs, and extremely fast server response. On the other hand, it also has some limitations, like 1GB storage and 10GB bandwidth per month, and caused me some problems with adding custom domain, but overall, the pros outweigh the cons.

Now, sadly, I'm leaving Firebase for Netlify, maybe just for a while, because beside having all my wish list items, it also offers:

- 100GB storage and 100GB bandwidth per month for free accounts.

- Let me upload my own certificates.

- Easier to setup custom subdomains.

- Integrate with Github/Gitlab, continuous deployment with a simple `git push`

100GB storage vs 1GB storage? And 10x more bandwidth? Yes please. 1GB free storage is a lot. One blog post with images is like 200KB on average, so 1GB is enough to store 5000 blog posts. If I write one blog every week (I don't write that much), or 50 blogs per year, 1GB will be enough for me to write in 100 years. However, Firebase stores the entire static site for each version. That means if I have 10 versions, each version only has about 100MB storage. So I have to *manually* delete older versions. That's too much work! Why do I have to worry about it in the first place anyway? And 10GB bandwidth seems like a lot, but divide 30 days, you're left with about 300MB transfer per day. Now maybe I'm a bit paranoid, but I don't like my visitors to be tracked by Google or other big companies, so I self-hosts all my web fonts and CSS instead of using a CDN. It is stupid and it slows down my website but it's my happy little mess. So 300MB a day to transfer all those fonts and CSS is perhaps not enough for me. I've never touch its limit, but I'm a bit prepping here, always calculate ahead.

Welllll what's the point of free signing certificate if you can't fuck things up, like provide your own RSA key instead of letting Let's Encrypt generate it for you? I've notice some RSA public keys have the same first few digits in their modulus N, like `bb e8` or `aa 12`. If I can generate two primes p and q such that N = p*q has most significant bits like`aa aa` then my certificate is like a shiny Pokémon, 1 in 4096 chance. Yeah baby I want to fuck things up like that. Nah that's a weird and maybe horrible idea (I'm using it though). The point here is that although Firebase provides automatic SSL,  it uses RSA 2048-bit, which is like barely minimum security. Your ECDH key exchange and AES 128-bit encryption has the strength of 128-bit, but RSA 2048-bit is like 112-bit strength, so RSA 3072-bit should be used here, but Firebase doesn't offer it. With custom certificate, although it's not automatically renewed, I can provide a RSA 4096-bit certificate for my site every 2-3 months, for better security and extra points on [tls.imirhil.fr](https://tls.imirhil.fr) CryptCheck.

<img src="/static/images/tls-scores.png" alt="tls-scores" class="img-responsive center-block"/>

I have to mention that connecting a custom subdomain with Netlify is easier than with Firebase. Only a CNAME is needed. Firebase requires a TXT record, but since I am using Gitlab Pages to host at my naked domain, I have an A record points to Gitlab server, and somehow it doesn't allow me to upload TXT record. I waited for 4 days and then found a way to fix it: temporary delete A record to add a TXT record so that Firebase can verify my ownership. That's some weird Google shit requirement to prove ownership. Even worse, Firebase doesn't let me do the alternative verification like HTML file upload or HTML tag.

Finally, after using Gitlab Pages for a while, I like CI a lot, and I miss this feature in Firebase. I haven't tracked my website since I made many modifications to plugins and theme, because all I need with Firebase is the `public` folder. With Netlify, I can go back to my old habit `git push` again. Its build time is also significantly faster than Gitlab Pages': 30 seconds vs 3 minutes.

So that's why I ditch Firebase for Netlify. Nevertheless, I'm not 100% satisfied. Netlify's server is preeeety slow compare to Firebase, like 200ms slower. It doesn't remove trailing slash in URLs, although "Prettify URLs" option is checked. <del>It uses a CDN to fasten things up, but only for images, and not CSS or JS (?). Maybe I should remove my tinfoil hat and use Google CDN to obtain faster speed instead. Someday, maybe, but not now.</del>

<ins>UPDATE Nov 09: after changing to relative URLs instead of absolute URLs, Netlify uses CDN to serve my site's CSS and JS. It turns out because my hard-coded responsive images use relative URLs, Netlify CDN-es them.</ins>
